package ccu.pllab.tcgen.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Arrays;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.junit.launcher.JUnitLaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageableElement;
import org.json.JSONArray;
import org.json.JSONObject;

public class PluginCore {
	private PluginCore () {}
	
	public static void addTastcaseFolderToSrcPath(URL path,
			IProject activeProject) throws CoreException, JavaModelException {
		if (activeProject != null) {
			IJavaProject javaProject = null;

			if (activeProject.hasNature(JavaCore.NATURE_ID)) {
				javaProject = JavaCore.create(activeProject);
			} else {
				// TODO: issue error message
			}

			IClasspathEntry[] origin_entries = javaProject.getRawClasspath();
			IClasspathEntry[] new_entries = new IClasspathEntry[origin_entries.length + 1];
			System.arraycopy(origin_entries, 0, new_entries, 0,
					origin_entries.length);
			IPath path_include_test_case = javaProject.getPath().append(path.getPath().replace(activeProject.getLocation().toString(), ""));
			IClasspathEntry path_entry_include_test_case = JavaCore
					.newSourceEntry(path_include_test_case, null);

			new_entries[origin_entries.length] = path_entry_include_test_case;

			if (!Arrays.asList(origin_entries).contains(path_entry_include_test_case)) {
					javaProject.setRawClasspath(new_entries, null);
			}
		}
		activeProject.refreshLocal(IProject.DEPTH_INFINITE, null);
	}
	
	public static void runJunit(IContainer active,TreeSelection selection) {
		int i=0;
		if(active!=null){
			IResource check_exist=active.findMember("/test-src");
			JUnitLaunchShortcut jUnitLaunchShortcut = new JUnitLaunchShortcut();
			if(check_exist!=null)
				{
				IFolder activeFolder=(IFolder)check_exist;
				TreeSelection selection_temp=null;
				IResource active_temp=null;
				switch(selection.getFirstElement().getClass().getSimpleName()){
					case "PackageImpl":
						org.eclipse.uml2.uml.Package pack =(org.eclipse.uml2.uml.Package)selection.getFirstElement();
						for(PackageableElement element:pack.getPackagedElements()){
							if(element.getClass().getSimpleName().equals("ClassImpl")){
								org.eclipse.uml2.uml.Class Cla=(org.eclipse.uml2.uml.Class)element;
								for(Operation Ope:Cla.getOperations()){
									active_temp=activeFolder.findMember("/tcgen/"+Cla.getName().toLowerCase()
											+Ope.getName()+"/"+Ope.getClass_().getName()+Ope.getName()+".java");
									if(active_temp!=null){
										selection_temp = new TreeSelection(new TreePath(new Object[]{active_temp}));
										jUnitLaunchShortcut.launch(selection_temp, "run");
									}
								}
							}
						}
						break;
					case "ClassImpl":
						org.eclipse.uml2.uml.Class Cla=(org.eclipse.uml2.uml.Class)selection.getFirstElement();
						for(Operation Ope:Cla.getOperations()){
							active_temp=activeFolder.findMember("/tcgen/"+Cla.getName().toLowerCase()+Ope.getName()
									+"/"+Ope.getClass_().getName()+Ope.getName()+".java");
							if(active_temp!=null){
								selection_temp = new TreeSelection(new TreePath(new Object[]{active_temp}));
								jUnitLaunchShortcut.launch(selection_temp, "run");
							}
						}
						break;
					case "OperationImpl":
						org.eclipse.uml2.uml.Operation Ope=(org.eclipse.uml2.uml.Operation)selection.getFirstElement();
						active_temp=activeFolder.findMember("/tcgen/"+Ope.getClass_().getName().toLowerCase()
								+Ope.getName()+"/"+Ope.getClass_().getName()+Ope.getName()+".java");
						if(active_temp!=null){
							selection_temp = new TreeSelection(new TreePath(new Object[]{active_temp}));
							jUnitLaunchShortcut.launch(selection_temp, "run");
						}
						break;
				}
			}
		//JUnitLaunchShortcut jUnitLaunchShortcut1 = new JUnitLaunchShortcut();
		
		//jUnitLaunchShortcut1.launch(selection, "run");
		}
	}
	
	public static void runCore(URL umlResourceFileURL, URL oclURL,
			URL facade_config_url, URL umlModelUrl, URL log4jPropertyURL,
			URL coreJarURL, URL outputFolderURL) throws IOException {
		String[] args = new String[] { "java",
				"-Declipse.directory=/opt/eclipseclp/",
				"-jar", coreJarURL.getFile(),
				"-uml_model", umlModelUrl.getFile(),
				"-ocl_model", oclURL.getFile(),
				"-output_folder", outputFolderURL.getFile(),
				"-log4j_properties", log4jPropertyURL.getFile(),
				"-uml_resource", umlResourceFileURL.getFile(),
				"-config", facade_config_url.getFile()};
		
		Process p = new ProcessBuilder(args).start();
		System.out.println("message from process stderr");
		writerInputStream(p.getErrorStream());
	}
	
	private static void writerInputStream(InputStream inputStream) throws IOException {
		InputStreamReader isr = new InputStreamReader(inputStream);
		BufferedReader br = new BufferedReader(isr);
		PrintWriter pw = new PrintWriter(System.out);
		String line;
		while ((line = br.readLine()) != null) {
			pw.println(line);
		}
		//pw.close();
		//br.close();
		//isr.close();
	}
	
	public static File configCreate(TreeSelection selection,String path_coverage){ //selection 
		File outconfigfile = null;
		try{
		JSONArray outconfig=new JSONArray();
		JSONObject associations=new JSONObject();
		JSONObject objects=new JSONObject();
		switch(selection.getFirstElement().getClass().getSimpleName()){ // select method or class or package
			case "PackageImpl":
				System.out.println("package");
				org.eclipse.uml2.uml.Package pack=(org.eclipse.uml2.uml.Package)selection.getFirstElement();
				for(PackageableElement element:pack.getPackagedElements()){
					switch(element.getClass().getSimpleName()){
						case "ClassImpl":
							objects.put(element.getName(),new JSONObject().put("range_of_instances","0..10"));
							break;
						case "AssociationImpl":
							associations.put(element.getName(),new JSONObject().put("range_of_instances","0..10"));
							break;
					}
				}
				for(PackageableElement element:pack.getPackagedElements()){
					if(element.getClass().getSimpleName().equals("ClassImpl")){
						org.eclipse.uml2.uml.Class Cla=(org.eclipse.uml2.uml.Class)element;
						for(Operation Ope:Cla.getOperations()){
							if(!Ope.getName().startsWith("set")&&!Ope.getName().startsWith("get")){
							JSONObject temp=new JSONObject();
								temp.put("associations",associations);
								temp.put("objects",objects);
								temp.put("int_domain", "0..500");
								temp.put("automatic_test_case_generation", true);
								temp.put("target_class", pack.getName()+"."+Cla.getName());
								temp.put("target_method", Ope.getName());
								temp.put("solving_timeout", 60);
								temp.put("fail_trial_time",2);
								temp.put("path_coverage", path_coverage);
								temp.put("invalid_case", false);
								outconfig.put(temp);
							}
						}
					}
				}
				break;
			case "ClassImpl":
				System.out.println("class");
				org.eclipse.uml2.uml.Class Cla=(org.eclipse.uml2.uml.Class)selection.getFirstElement();
				for(PackageableElement element:Cla.getPackage().getPackagedElements()){
					switch(element.getClass().getSimpleName()){
						case "ClassImpl":
							objects.put(element.getName(),new JSONObject().put("range_of_instances","0..10"));
							break;
						case "AssociationImpl":
							associations.put(element.getName(),new JSONObject().put("range_of_instances","0..10"));
							break;
					}
				}
				for(Operation Ope:Cla.getOperations()){
					if(!Ope.getName().startsWith("set")&&!Ope.getName().startsWith("get")){
					JSONObject temp=new JSONObject();
						temp.put("associations",associations);
						temp.put("objects",objects);
						temp.put("int_domain", "0..500");
						temp.put("automatic_test_case_generation", true);
						temp.put("target_class", Cla.getPackage().getName()+"."+Cla.getName());
						temp.put("target_method", Ope.getName());
						temp.put("solving_timeout", 60);
						temp.put("fail_trial_time",2);
						temp.put("path_coverage", path_coverage);
						temp.put("invalid_case", false);
						outconfig.put(temp);
					}
				}
				break;
			case "OperationImpl":
				System.out.println("method");
				org.eclipse.uml2.uml.Operation Ope=(org.eclipse.uml2.uml.Operation)selection.getFirstElement();
				for(PackageableElement element:Ope.getClass_().getPackage().getPackagedElements()){
					switch(element.getClass().getSimpleName()){
						case "ClassImpl":
							objects.put(element.getName(),new JSONObject().put("range_of_instances","0..10"));
							break;
						case "AssociationImpl":
							associations.put(element.getName(),new JSONObject().put("range_of_instances","0..10"));
							break;
					}
				}
				if(!Ope.getName().startsWith("set")&&!Ope.getName().startsWith("get")){
					JSONObject temp=new JSONObject();
					temp.put("associations",associations);
					temp.put("objects",objects);
					temp.put("int_domain", "0..500");
					temp.put("automatic_test_case_generation", true);
					temp.put("target_class", Ope.getClass_().getPackage().getName()+"."+Ope.getClass_().getName());
					temp.put("target_method", Ope.getName());
					temp.put("solving_timeout", 60);
					temp.put("fail_trial_time",2);
					temp.put("path_coverage", path_coverage);
					temp.put("invalid_case", false);
					outconfig.put(temp);
				}
				break;
		}
		outconfigfile=File.createTempFile("temp", ".json");
		outconfigfile.deleteOnExit();
		FileWriter writer = new FileWriter(outconfigfile);
		writer.write(outconfig.toString());
		writer.close();
		}catch(Exception e){
			outconfigfile = null;
		}
		return outconfigfile;
	}
}
