package ccu.pllab.tcgen.plugin.popup.actions;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import ccu.pllab.tcgen.plugin.PluginCore;

public class TCRun implements IObjectActionDelegate
{

	@Override
	public void run(IAction arg0)
	{
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchPage page = workbench.getActiveWorkbenchWindow()
				.getActivePage();
		IEditorPart editorPart = page.getActiveEditor();
		IProject activeProject = null;
		IContainer activeFolder=null;
		if (editorPart != null) {
			IFileEditorInput input = (IFileEditorInput) editorPart
					.getEditorInput();
			IFile file = input.getFile();
			activeProject = file.getProject();
			activeFolder =file.getParent();
			
		}
		//PluginCore.runJunit(activeProject);
		PluginCore.runJunit(activeFolder,(TreeSelection)page.getSelection());

	}

	@Override
	public void selectionChanged(IAction arg0, ISelection arg1)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void setActivePart(IAction arg0, IWorkbenchPart arg1)
	{
		// TODO Auto-generated method stub

	}

}
