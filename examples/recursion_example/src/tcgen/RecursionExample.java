package tcgen;

public class RecursionExample {
	public int fibonacci(int num) throws NumberRangeException {
		if (num < 0) {
			throw new NumberRangeException();
		}
		if (num == 0) {
			return 0;
		} else if (num == 1) {
			return 1;
		} else {
			return fibonacci(num - 2) + fibonacci(num - 1);
		}
	}

	public int factorial(int num) throws NumberRangeException {
		if (num < 0) {
			throw new NumberRangeException();
		}
		if (num == 0) {
			return 1;
		} else {
			return num * factorial(num - 1);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RecursionExample) {
			return true;
		}
		return false;
	}
}
