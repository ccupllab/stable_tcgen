package tcgen;

import java.util.HashSet;

public class Teacher {
	
	// associations
	HashSet<Student> students = new HashSet<Student>();
	HashSet<Laboratory> laboratory = new HashSet<Laboratory>();
	private int id;
	
	// getter/setter
	public HashSet<Student> getStudents() {
		return students;
	}
	public void setStudents(HashSet<Student> students) {
		this.students = students;
	}
	public HashSet<Laboratory> getLaboratory() {
		return laboratory;
	}
	public void setLaboratory(HashSet<Laboratory> laboratory) {
		this.laboratory = laboratory;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teacher other = (Teacher) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
