package tcgen;

public class Date {

	private int year;
	private int month;
	private int day;

	public Date() {
	}

	public Date(int y, int m, int d) throws DateErrorException {	
		if ((m == 1) || (m == 3) || (m == 5) || (m == 7) || (m == 8)
				|| (m == 10) || (m == 12)) {
			if (d >= 32) {
				throw new DateErrorException();
			}
		} else if ((m == 4) || (m == 6) || (m == 9) || (m == 11)) {
			if (d >= 31) {
				throw new DateErrorException();
			}
		} else if (((y % 400) == 0) || ((y % 4) == 0 && (y % 100) != 0)) {
			if (d >= 30) {
				throw new DateErrorException();
			}
		} else if (d >= 29) {
			throw new DateErrorException();
		}
		if (y < 1 || y > 3999 || m < 1 || m > 12 || d < 1) {
			throw new DateErrorException();
		}
		this.year = y;
		this.month = m;
		this.day = d;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public Date next() {
		if (month == 12) {
			if (day == 31) {
				Date return_date = new Date();
				return_date.setYear(year + 1);
				return_date.setMonth(1);
				return_date.setDay(1);
				return return_date;
			} else {
				Date return_date = new Date();
				return_date.setYear(year);
				return_date.setMonth(month);
				return_date.setDay(day + 1);
				return return_date;
			}
		} else {
			if ((month == 1) || (month == 3) || (month == 5) || (month == 7)
					|| (month == 8) || (month == 10)) {
				if (day == 31) {
					Date return_date = new Date();
					return_date.setYear(year);
					return_date.setMonth(month + 1);
					return_date.setDay(1);
					return return_date;
				} else {
					Date return_date = new Date();
					return_date.setYear(year);
					return_date.setMonth(month);
					return_date.setDay(day + 1);
					return return_date;
				}
			} else {
				if ((month == 4) || (month == 6) || (month == 9)
						|| (month == 11)) {
					if (day == 30) {
						Date return_date = new Date();
						return_date.setYear(year);
						return_date.setMonth(month + 1);
						return_date.setDay(1);
						return return_date;
					} else {
						Date return_date = new Date();
						return_date.setYear(year);
						return_date.setMonth(month);
						return_date.setDay(day + 1);
						return return_date;
					}
				} else {
					if ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0)) {
						if (day == 29) {
							Date return_date = new Date();
							return_date.setYear(year);
							return_date.setMonth(month + 1);
							return_date.setDay(1);
							return return_date;
						} else {
							Date return_date = new Date();
							return_date.setYear(year);
							return_date.setMonth(month);
							return_date.setDay(day + 1);
							return return_date;
						}
					} else {
						if (day == 28) {
							Date return_date = new Date();
							return_date.setYear(year);
							return_date.setMonth(month + 1);
							return_date.setDay(1);
							return return_date;
						} else {
							Date return_date = new Date();
							return_date.setYear(year);
							return_date.setMonth(month);
							return_date.setDay(day + 1);
							return return_date;
						}
					}
				}
			}
		}
	}

	public boolean equals(Date obj) {
		if (day != obj.day)
			return false;
		if (month != obj.month)
			return false;
		if (year != obj.year)
			return false;
		return true;
	}

	public int dayCount() {

		int thisYear, priorYear;
		thisYear = (month - 1) * 31 + day;
		if (month > 2) {
			thisYear = thisYear - (((4 * month) + 23) / 10);
			if ((((year % 4) == 0) && ((year % 100) != 0))
					|| ((year % 400) == 0)) {
				thisYear = thisYear + 1;
			}
		}
		priorYear = 365 * (year - 1) + (year - 1) / 4 - (year - 1) / 100
				+ (year - 1) / 400;
		return thisYear + priorYear;

	}

	public int dayDiff(Date date) {
		if (this.dayCount() >= date.dayCount()) {
			return this.dayCount() - date.dayCount();
		} else {
			return date.dayCount() - this.dayCount();
		}
	}

}
