package tcgen;

import java.util.HashSet;

public class Student {

	// assocations
	HashSet<Teacher> instructor = new HashSet<Teacher>();
	HashSet<Laboratory> laboratory = new HashSet<Laboratory>();
	private int id;
	
	// getter/setter
	public HashSet<Teacher> getInstructor() {
		return instructor;
	}
	public void setInstructor(HashSet<Teacher> instructor) {
		this.instructor = instructor;
	}
	public HashSet<Laboratory> getLaboratory() {
		return laboratory;
	}
	public void setLaboratory(HashSet<Laboratory> laboratory) {
		this.laboratory = laboratory;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
