:-lib(apply).
:-lib(apply_macros).
:-lib(lists).
:-lib(listut).
:-lib(ic).

ocl_iterate(Instances, Vars, CollectionPredicate, AccInitPredicate, AccIterPredicate, Result) :- 
	apply(CollectionPredicate, [Instances, Vars, Collection]),
	apply(AccInitPredicate, [Instances, Vars, AccInitValue]),
	ocl_iterate_helper(Instances, Vars, Collection, AccInitValue, AccIterPredicate, Result).
	
delay ocl_iterate_helper(_, _, Collection, _, _, _) if var(Collection).
ocl_iterate_helper(_, _, Collection, AccInitValue, _, AccInitValue) :- length(Collection, 0).
ocl_iterate_helper(Instances, Vars, Collection, AccInitValue, AccIterPredicate, Result) :- 
	( 
	  foreach(Elem, Collection),   
	  fromto(AccInitValue,AccPre,AccPost, Result),
	  param(Instances, Vars, AccIterPredicate)
        do
	  append(Vars, [[AccPre, AccPre], [Elem, Elem]], NewVars),
	  apply(AccIterPredicate, [Instances, NewVars, AccPost]) 
	).
	
iterate_test(_, [[AccPre, AccPre], [Elem, Elem]], Result) :-
	Result #= Elem + AccPre.

variable_state("precondition", [Pre, _], Pre).
variable_state("postcondition", [_, Post], Post).
variable_state("both", [Pre, _], Pre).

instance_state("precondition", [InstancePre, _], InstancePre).
instance_state("postcondition", [_, InstancePost], InstancePost).
instance_state("both", [InstancePre, _], InstancePre).

ocl_if(Instances, Vars, ConditionPredicate, ThenPredicate, ElsePredicate, Result) :-
        apply(ConditionPredicate, [Instances, Vars, Condition]),
        ocl_if_delay(Instances, Vars, Condition, ThenPredicate, ElsePredicate, Result).
        
delay ocl_if_delay(_, _, Condition, _, _, _) if nonground(Condition).         
ocl_if_delay(Instances, Vars, Condition, ThenPredicate, ElsePredicate, Result) :-
	(
        Condition = 1 ->
              apply(ThenPredicate, [Instances, Vars, Result]);
              (Condition = 0, apply(ElsePredicate, [Instances, Vars, Result]))
    ).

is_collection(L) :-
	is_list(L),
	nth1(1, L, E),
	(uml_obj ~= E),
	(uml_asc ~= E).


ocl_set_at(Collection, Index, Elem) :-
	ocl_sequence_at(Collection, Index, Elem).
	
stateSync(Pre, Post) :-
	length(Pre, N),
	(count(I, 1, N), param(Pre, Post) do
		nth1(I, Pre, PreInstances), nth1(I, Post, PostInstances),
		(
			((var(PostInstances), eclipse_language:delayed_goals_number(PostInstances, DN), DN = 0) -> PostInstances = PreInstances);
			(instanceType(I, uml_obj) -> syncClass(PreInstances, PostInstances); true)
		)
	).
	
syncClass(PreInstances, PostInstances) :-
	length(PreInstances, N),
	(count(I, 1, N), param(PreInstances, PostInstances) do
		nth1(I, PreInstances, PreObj), nth1(I, PostInstances, PostObj),
		syncObj(PreObj, PostObj)
	).

syncObj(Pre, Post) :-
	length(Pre, N),
	(count(I, 1, N), param(Pre, Post) do
		nth1(I, Pre, PreAtt), nth1(I, Post, PostAtt),
		((var(PostAtt), not is_solver_var(PostAtt)) -> PostAtt = PreAtt); true
	).

delay gen_seq_from_range(Start, Last, _) if (var(Start); var(Last)).	
gen_seq_from_range(Start, Last, Result) :-
	Domain :: Start..Last,
	get_domain_as_list(Domain, Result).
	
ocl_string_equals(_, _, X, Y, Result) :-
	X=Y,
	Result#=1.
   
ocl_string_equals(_,_, X, Y, Result) :-
	X\=Y,
	Result#=0.
	
ocl_string_not_equels(_,_, X, Y, Result):-
	( X \= Y -> Result #= 1 ; Result #= 0).
   
ocl_string_size(_,_, X, Result) :-
	string_length( X, Result).   
   
ocl_string_concat(_,_, X, Y, Result) :-
	append_strings( X, Y, Result).
   
ocl_string_substring(_,_, X, Y, Z, Result):-
	M #= Z - Y +1,
	substring(X, Y, M, Result).
	
ocl_string_toInteger(_,_, X, Result):-
	number_string(Result,X),
	integer(Result).
	
ocl_string_toReal(_,_, X, Result):-
	number_string(P,X),
	Result $= float(P),
	real(Result).
	
ocl_string_toBoolean(_,_, X, Result):-
	X="true",
	Result#=1.
	
ocl_string_toBoolean(_,_, X, Result):-
	X="false",
	Result#=0.
	
ocl_string_indexOf(_,_, X, Y, Result):-
	(substring(X, Y, P) -> Result #= P ; Result #= 0).

ocl_string_toUpperCase(_,_, X, Result):-
	toUpperCase(X,Result).

toUpperCase(N,X):-
	string_list(N,A),
	subtoUpperCase(A,B),
	string_list(X,B).
	
subtoUpperCase([],[]).
subtoUpperCase([A|B],[C|D]):-
	clrbit(A,5,C),
	subtoUpperCase(B,D).		

ocl_string_toLowerCase(_,_, X, Result):-
	toLowerCase(X,Result).

toLowerCase(N,X):-
	string_list(N,A),
	subtoLowerCase(A,B),
	string_list(X,B).
	
subtoLowerCase([],[]).
subtoLowerCase([A|B],[C|D]):-
	clrbit(A,5,C),
	subtoLowerCase(B,D).	

ocl_string_equalsIgnoreCase(_,_, X, Y, Result):-
	toUpperCase(X,XUC),
	toUpperCase(Y,YUC),
	(XUC=YUC -> Result #=1 ; Result #=0).
	
ocl_string_at(_,_, X, Y, Result):-
	substring(X, Y, 1, Result).
	
ocl_string_characters(_,_, X, Result):-
	string_list(X, P),
	int2char(P,Result).
	
int2char([],[]).
int2char([A|P],[B|Result]):-
	char_int(A,B),
	int2char(P,Result).
	
parameterOfCollection(_, [_|_]).
parameterOfSequence(_, Seq) :- parameterOfCollection(_, Seq).
parameterOfSet(_, Set) :- parameterOfCollection(_, Set).
parameterOfOrderSet(_, Set) :- parameterOfCollection(_, Set).
parameterOfBag(_, Bag) :- parameterOfCollection(_, Bag).
