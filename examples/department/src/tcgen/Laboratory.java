package tcgen;

import java.util.HashSet;

public class Laboratory {

	HashSet<Student> students = new HashSet<Student>();
	HashSet<Teacher> instructor = new HashSet<Teacher>();
	int id;

	public Laboratory() {
		this.limit = new Integer(0);
	}

	public Laboratory(int i, int l, Teacher teacher) {
		this();
		this.instructor.add(teacher);
		this.id = i;
		this.limit = l;
	}

	Integer limit = 0;

	public Boolean canRegister(Student student) {
		if (this.students.size() == this.limit || students.contains(student))
			return false;

		return true;
	}

	public void register(Student student) throws RegisterException {
		if (this.students.size() == this.limit
				|| this.students.contains(student)) {
			throw new RegisterException();
		}
		this.students.add(student);
	}

	// getter/setter
	public HashSet<Student> getStudents() {
		return students;
	}

	public void setStudents(HashSet<Student> students) {
		this.students = students;
	}

	public HashSet<Teacher> getInstructor() {
		return instructor;
	}

	public void setInstructor(HashSet<Teacher> instructor) {
		this.instructor = instructor;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
//		if (limit <= 0) {
//			throw new LimitException();
//		}
		this.limit = limit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((limit == null) ? 0 : limit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Laboratory other = (Laboratory) obj;
		if (id != other.id)
			return false;
		if (limit == null) {
			if (other.limit != null)
				return false;
		} else if (!limit.equals(other.limit))
			return false;
		return true;
	}

	public void setId(int id) {
		this.id = id;
	}
}
