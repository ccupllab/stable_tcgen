package tcgen;

public class IntegerRange {

	private int first;
	private int last;
	
	public void setFirst(int f) {
		this.first = f;
	}
	
	public void setLast(int l) {
		this.last = l;
	}
	
	public int sum() {
		int result = 0;
		for (int i = first; i <= last; i++) {
			result += i;
		}
		return result;
	}
	
	public int sumEven() {
		int result = 0;
		for (int i = first; i <= last; i++) {
			if (i % 2 == 0) {
				result += i;
			}
			
		}
		return result;
	}

}
