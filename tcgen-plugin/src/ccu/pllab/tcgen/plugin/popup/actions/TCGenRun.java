package ccu.pllab.tcgen.plugin.popup.actions;

import java.io.File;
import java.net.URL;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import ccu.pllab.tcgen.plugin.Activator;
import ccu.pllab.tcgen.plugin.PluginCore;

public class TCGenRun implements IObjectActionDelegate
{

	@Override
	public void run(IAction arg0)
	{
		try {
			String path_coverage;
			switch(arg0.getText()){//get select name
				case "Use All Decision Coverage":
					path_coverage="adc";
					break;
				case "Use All Decision/Condition Coverage":
					path_coverage="adcc";
					break;
				case "Use Multiple Condition Coverage":
					path_coverage="mcc";
					break;
				default:
					path_coverage="adcc";
					break;
			}
			URL umlResourceFileURL = FileLocator.resolve(
					Activator.getDefault().getBundle()
					.getEntry("/libs/org.eclipse.uml2.uml.resources_3.1.0.v201005031530.jar"));
			URL coreJarURL = FileLocator.resolve(
					Activator.getDefault().getBundle()
							.getEntry("/libs/tcgen.jar"));
			URL log4jPropertyURL = FileLocator.resolve(Activator.getDefault()
					.getBundle().getEntry("/log4j.properties"));
			
			
			
			IWorkbench workbench = PlatformUI.getWorkbench();
			IWorkbenchPage page = workbench.getActiveWorkbenchWindow()
					.getActivePage();
			TreeSelection select_for_UML=(TreeSelection) page.getSelection();
			File config_temp=PluginCore.configCreate(select_for_UML,path_coverage);
			
			IWorkbenchPart workbenchPart = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage().getActivePart();
			IFile modelURI = (IFile) workbenchPart.getSite().getPage()
					.getActiveEditor().getEditorInput().getAdapter(IFile.class);
			
			URL oclURL = new URL("file:"
					+ modelURI.getLocationURI().getPath()
							.replace(".uml", ".ocl"));
			URL facade_config_url = new URL("file:"+ config_temp.getPath()); 
				//	+ modelURI.getLocationURI().getPath()   
				//			.replace(".uml", ".config.json"));
			URL umlModelUrl = modelURI.getLocationURI().toURL();
			String output_folder_name = "test-src";
			URL outputFolderURL = new URL("file:" + modelURI.getParent().getLocation() + java.io.File.separatorChar + output_folder_name);
			PluginCore.runCore(umlResourceFileURL, oclURL, facade_config_url, umlModelUrl,
					log4jPropertyURL, coreJarURL, outputFolderURL);
			IEditorPart editorPart = page.getActiveEditor();
			IProject activeProject = null;
			IContainer activeFolder=null;
			if (editorPart != null) {
				IFileEditorInput input = (IFileEditorInput) editorPart
						.getEditorInput();
				IFile file = input.getFile();
				activeProject = file.getProject();
				activeFolder=file.getParent();
			}
			PluginCore.addTastcaseFolderToSrcPath(outputFolderURL, activeProject);
			PluginCore.runJunit(activeFolder,(TreeSelection)page.getSelection());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void selectionChanged(IAction arg0, ISelection arg1)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setActivePart(IAction arg0, IWorkbenchPart arg1)
	{
		// TODO Auto-generated method stub
		
	}

}
