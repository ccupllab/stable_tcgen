package tcgen;

public class Triangle {
	private int sideA;
	private int sideB;
	private int sideC;

	public Triangle() {

	}

	public Triangle(int sa, int sb, int sc) throws EdgeErrorException, EdgeNumberException {
		if (sa <= 0 || sb <= 0 || sc <= 0||sa + sb <= sc || sb + sc <= sa || sa + sc <= sb) {
			throw new EdgeErrorException();
		}
		this.sideA = sa;
		this.sideB = sb;
		this.sideC = sc;
	}

	public void setSideA(Integer sa) {
		sideA = sa;
	}

	public void setSideB(Integer sb) {
		sideB = sb;
	}

	public void setSideC(Integer sc) {
		sideC = sc;
	}

	public int getSideA() {
		return sideA;
	}

	public int getSideB() {
		return sideB;
	}

	public int getSideC() {
		return sideC;
	}

	public String category() {
		if (sideA == sideB) {
			if (sideB == sideC) {
				return "Equilateral";
			} else {
				return "Isosceles";
			}
		} else {
			if (sideA == sideC) {
				return "Isosceles";
			} else {
				if (sideB == sideC) {
					return "Isosceles";
				} else {
					return "Scalene";
				}
			}
		}
	}

	public boolean equals(Triangle triangle) {
		if (triangle.getSideA() != sideA)
			return false;
		else if (triangle.getSideB() != sideB)
			return false;
		else if (triangle.getSideC() != sideC)
			return false;
		else
			return true;

	}
}
